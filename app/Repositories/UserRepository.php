<?php

namespace App\Repositories;

use App\Dtos\UserDto;
use Illuminate\Redis\Connections\PhpRedisConnection;

class UserRepository
{
    public function findOrDefault($id)
    {
        $result = json_decode(\LRedis::get('truckto:user:'.$id));
        if ($result === null) return null;
        return new UserDto((array) $result);
    }

    public function insert(UserDto $userDto)
    {
        $userDto->id = $this->getNextId();
        \LRedis::set('truckto:user:', json_encode($userDto->toArray()));
        $this->updateNextId();
    }

    private function getNextId()
    {
        return (integer) (\LRedis::get('truckto:user:next') ?? 1);
    }

    private function updateNextId()
    {
        $lastId = $this->getNextId();
        \LRedis::set('truckto:user:next', ''.($lastId + 1));
    }
}