<?php

namespace App\Dtos;

abstract class Dto
{
    /**
     * Dto constructor.
     * @param array $properties Add properties as array for this Dto
     */
    public function __construct(array $properties)
    {
        $this->assign($properties);
    }

    /**
     * Assign more properties using array
     * @param array $properties
     */
    public function assign(array $properties)
    {
        foreach ($properties as $property => $value){
            if (property_exists(static::class, $property)){
                $this->$property = $value;
            }
        }
    }

    /**
     * Convert this object to Array
     * @return array
     */
    public function toArray()
    {
        return (array)$this;
    }
}