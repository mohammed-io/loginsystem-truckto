<?php

namespace App\Dtos;

use Illuminate\Contracts\Auth\Authenticatable;

class UserDto extends Dto implements Authenticatable
{
    /**
     * @var int $id
     */
    public $id;

    /**
     * @var string $personal_id
     */
    public $personal_id;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string $email
     */
    public $email;

    /**
     * @var string $password_hash
     */
    public $password_hash;

    /**
     * @var string $phone_number
     */
    public $phone_number;

    public function __construct(array $properties)
    {
        parent::__construct($properties);
    }

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return "id";
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->id;
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password_hash;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return "";
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    public function setRememberToken($value)
    {

    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return "";
    }
}